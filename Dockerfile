FROM mysql:5.6

MAINTAINER Alexander Guerrero <alex.guelu@gmail.com>

COPY 01.-Scripts.sql /docker-entrypoint-initdb.d